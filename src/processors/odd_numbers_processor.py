import models
import services


class OddNumbersProcessor:
    def __init__(self, repo: services.OddComputer):
        self.repo = repo

    async def process(self, numbers: models.Numbers):
        return await self.repo.compute_odd_sum(numbers)
