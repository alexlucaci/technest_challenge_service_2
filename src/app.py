import fastapi

from fastapi.exceptions import RequestValidationError
from starlette.responses import JSONResponse
from starlette.exceptions import HTTPException as StarletteHTTPException

import api

app = fastapi.FastAPI()
app.include_router(api.api_router)

odd_sum_api = fastapi.FastAPI(openapi_prefix="/oddsum")
odd_sum_api.include_router(api.odd_sum_router)

app.mount("/oddsum", odd_sum_api)


@app.exception_handler(RequestValidationError)
@odd_sum_api.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    message = exc.errors()[0]["msg"]
    return JSONResponse({"message": message}, status_code=400)


@app.exception_handler(StarletteHTTPException)
@odd_sum_api.exception_handler(StarletteHTTPException)
async def http_exception_handler(request, exc):
    return JSONResponse({"message": str(exc.detail)}, status_code=exc.status_code)
