from pydantic import BaseModel


class ValidResponse(BaseModel):
    data: dict
    message: str = "Response ok"
