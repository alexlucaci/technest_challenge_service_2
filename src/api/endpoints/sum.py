import fastapi
from pydantic import Required

import core
import models
import services
import processors

APP_SETTINGS = core.get_app_settings()

router = fastapi.APIRouter()


@router.post("/sum")
async def sum(numbers: models.Numbers = Required):
    repo = services.OddComputer(url=APP_SETTINGS.odd_service_url)
    processor = processors.OddNumbersProcessor(repo)
    data = await processor.process(numbers)
    return models.ValidResponse(data=data)
