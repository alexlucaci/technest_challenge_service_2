import models


class SumProcessor:
    @staticmethod
    def compute_sum(numbers: models.OddNumbers) -> dict:
        _sum = 0

        for number in numbers.numbers:
            _sum += number

        return {"sum": _sum}
