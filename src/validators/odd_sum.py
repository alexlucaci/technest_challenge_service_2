from .base_validator import BaseValidator


class OddSum(BaseValidator):
    sum: int
