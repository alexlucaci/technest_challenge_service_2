import fastapi

from api import endpoints

api_router = fastapi.APIRouter()

api_router.include_router(endpoints.sum_router)
# api_router.include_router(endpoints.odd_sum_router)
