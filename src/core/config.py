from functools import lru_cache

from pydantic import BaseSettings

# import os


class AppSettings(BaseSettings):
    # odd_service_url: str = os.environ['ODD_SERVICE_URL']
    odd_service_url: str = "http://localhost:55002"


@lru_cache()
def get_app_settings() -> AppSettings:
    return AppSettings()
