import validators
import models
from .http_service import HTTPService


class OddComputer(HTTPService):
    async def compute_odd_sum(self, numbers: models.Numbers) -> dict:
        payload = dict(numbers)
        response = await self.do_post(endpoint="/oddsum", payload=payload)
        response = validators.OddSum(response["data"]).data
        return response
