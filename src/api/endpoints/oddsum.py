import fastapi
from pydantic import Required

import core
import models
import processors

APP_SETTINGS = core.get_app_settings()

router = fastapi.APIRouter()


@router.post("/")
async def oddsum(numbers: models.OddNumbers = Required):
    processor = processors.SumProcessor()
    data = processor.compute_sum(numbers)
    return models.ValidResponse(data=data)
