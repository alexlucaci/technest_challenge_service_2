from typing import List
from pydantic import BaseModel, validator


class Numbers(BaseModel):
    numbers: List[int]

    @validator("numbers", pre=True)
    def is_valid_list_of_integers(cls, numbers: List[int]) -> List[int]:  # noqa: N805
        validation_message = "Not a valid list of integers"

        if len(numbers) < 2:
            raise ValueError(validation_message)
        for number in numbers:
            if type(number) is not int:
                raise ValueError(validation_message)

        return numbers


class OddNumbers(BaseModel):
    numbers: List[int]

    @validator("numbers", pre=True)
    def is_valid_list_of_odd_numbers(
        cls, numbers: List[int]
    ) -> List[int]:  # noqa: N805
        validation_message = "At least one of the numbers is even"

        for number in numbers:
            if number % 2 == 0:
                raise ValueError(validation_message)

        return numbers
